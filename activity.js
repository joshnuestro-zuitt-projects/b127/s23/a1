// Create a sing room
/*
INSERT "singles"
db.rooms.insertOne({
	"name":"singles",
	"accomodates":2,
	"price":1000,
	"description":"A simple room with all the basic necessities"
})

UPDATE "singles"
db.rooms.updateOne(
{
	"_id":ObjectId("613600215de88e0e8a64f346")
},
{
	$set: {
	"rooms_available":10,
	"isAvailable":false
	}
}
)

INSERT ROOMS "queen" AND "double"
db.rooms.insertMany([
{
	"name":"queen",
	"accomodates":4,
	"price":4000,
	"description":"A room with a queen sized bed perfect for a simple getaway",
	"rooms_available":15,
	"isAvailable":false
},
{
	"name":"double",
	"accomodates":3,
	"price":2000,
	"description":"A room fit for a small family going on a vacation",
	"rooms_available":5,
	"isAvailable":false
}
])

FIND "double"
db.rooms.find({"name":"double"})

UPDATE "queen"
db.rooms.updateOne(
{
    "name":"queen"
},
{
    $set: {
        "rooms_available":0
    }
}
)

DELETEMANY "rooms_available":0
db.rooms.deleteMany({
	"rooms_available":0
})
*/